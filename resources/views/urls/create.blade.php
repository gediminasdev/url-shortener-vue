@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New URL</div>

                <div class="card-body">
                    <url-create></url-create>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection