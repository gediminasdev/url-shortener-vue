@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Shortened URLs list') }}</div>

                <div class="card-body">
                    <a class="btn btn-primary" href="{{ route('urls.create') }}">Add new URL</a>
                    <br /><br />
                    <table class="table">
                        <thead>
                            <tr>
                                <th>URL</th>
                                <th>Short Code</th>
                                <th>Category</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($urls as $myurl)
                            <tr>
                                <td>{{ $myurl->url }}</td>
                                <td><a href="{{ route('home').'/'.$myurl->hash }}" target="_blank">{{ $myurl->hash }}</a></td>
                                <td>{{ $myurl->category->name }}</td>
                                <td>{{ $myurl->created_at }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3">{{ __('No URLs saved yet.') }}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection