# Užduotis #

Sukurti nuorodų trumpinimo tinklapio prototipą.

## Reikalavimai ##

1. Trumpinama nuoroda privalo būti validus URL.
2. Nuoroda sutrumpinama į 6 simbolių hash‘ą, kuris yra sudarytas iš alphanumeric simbolių.
3. Sutrumpinta nuoroda saugoma MySQL duomenų bazėje.
4. Tikrinama ar tokia nuoroda jau yra sutrumpinta ir jei taip – išduodamas tas pats hash‘as.
5. Sutrumpintos nuorodos formatas – example.com/[hash].
6. Submito metu nuoroda patikrinama [„Google Safe Browsing“ API](https://developers.google.com/safe-
browsing/v4/lookup-api). Arba kitame, tą pačią funkciją atliekančiame, API.
7. Įgyvendinti funkcionalumą, kad atidarius sutrumpintą nuorodą, vartotojas būtų nukreipiamas į originalią
nuorodą.

* Privalumas, jei funkcionalumas gali veikti iš katalogo (t.y. example.com/something/[hash]).
* Privalumas, jei užduotis atlikta su Laravel ir Vue.js.

Atlikus užduotį, reikia atsiųsti MySQL dump‘ą ir užduoties source code.

# Prototipas #

## Įdiegimas ##

* Clone the repository with `git clone` or download manually
* Copy .env.example file to .env and edit database credentials there
* Run composer install
* Run php artisan key:generate
* Run php artisan migrate --seed (it has some seeded data for your testing)
* Run npm install
* Run npm run dev

### Sistemos reikalavimai ###

* PHP >=7.3.x
* MySQL 5.7
* PHP Composer
* Google Safe Browsing API raktas

## Sprendimo komentarai ##

* Validacija siunčiant formą `App\Http\Requests\StoreUrlRequest` nuorodos laukeliui - url tipas ir Google Safe Browsing taisyklė. (reikalinga nustatyti SAFE_BROWSING_KEY reikšmę tarp .env)
* Hash generuojamas ir tikrinimas per `/api/hash` route.
* Išsaugoma duombazėje per `Api\UrlController`
