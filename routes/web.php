<?php

use Illuminate\Support\Facades\Route;
use App\Category;
use App\Url;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('urls', 'UrlController')->only([
    'index', 'create'
]);

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home');
Route::get('/', 'UrlController@index')->name('home');

Route::get('/{myCategory:name}/{url:hash}', function (Category $myCategory, Url $url) {
    return redirect()->away($url->url);
});

Route::get('/{myWebsite:hash}', function (Url $myWebsite) {
    return redirect()->away($myWebsite->url);
});
