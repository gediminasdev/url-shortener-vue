<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mycategories = ['Free', 'Basic', 'Premium'];

        foreach ($mycategories as $category) {
            \App\Category::create(['name' => $category]);
            // DB::table('categories')->insert(['name' => $category]);
        }
    }
}
