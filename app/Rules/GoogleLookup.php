<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;

class GoogleLookup implements Rule
{
    private $safeBrowsing;
    private $errMessage;
    public $url;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($urlFromTheForm = null)
    {
        $this->url = $urlFromTheForm;

        $this->safeBrowsing = Http::post('https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' . config('app.safeBrowsingKey'), [
            'client' => [
                'clientId' => 'Intus Windows LCC test',
                'clientVersion' => '1.0.0'
            ],
            'threatInfo' => [
                'threatTypes' => ['MALWARE', 'SOCIAL_ENGINEERING'],
                'platformTypes' => ['WINDOWS'],
                'threatEntryTypes' => ['URL'],
                'threatEntries' => [[
                    'url' => $urlFromTheForm
                ]]
            ]
        ]);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->errMessage = 'The :attribute is in a threat list in Google.';

        if (
            $this->safeBrowsing->successful()
            && count($this->safeBrowsing->json()) == false
        ) {
            return true;
        } elseif ($this->safeBrowsing->failed()) 
        {
            $this->errMessage = $this->safeBrowsing->body();
        } elseif ($this->safeBrowsing->clientError()
                    || $this->safeBrowsing->serverError()) 
        {
            $this->errMessage = 'Cannot check your :attribute in Google Safe Browsing. Please try again later.';
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errMessage;
    }
}
