<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\GoogleLookup;

class StoreUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $url = $this->url;
        return [
            'url' => [
                'url',
                'required',
                new GoogleLookup($url)
            ],
            'hash' => 'unique:App\Url,hash|required|size:6',
            'category_id' => 'required'
        ];
    }
}
