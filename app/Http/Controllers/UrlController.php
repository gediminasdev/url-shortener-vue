<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreUrlRequest;
use App\Url;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function index()
    {
        $urls = Url::with('category')->get();
        
        return view('urls.list', compact('urls'));
    }

    public function create()
    {
        $categories = Category::all();

        return view('urls.create', compact('categories'));
    }

    // public function store(StoreUrlRequest $request)
    // {
    //     Url::create($request->validated());

    //     return redirect()->route('urls.index');
    // }
}
