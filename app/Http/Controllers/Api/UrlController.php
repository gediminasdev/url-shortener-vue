<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUrlRequest;
use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UrlController extends Controller
{

    public function store(StoreUrlRequest $request)
    {
        return Url::firstOrCreate($request->validated());
    }

    public function generate()
    {
        do {
            $newHash = Str::random(6);
            $data = Url::all()->where('hash', $newHash);
        } while ($data->count());

        return response()->json(['newHash' => $newHash]);
    }
}
