<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
    protected $primaryKey = 'url';
    protected $fillable = ['category_id', 'url', 'hash'];
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
